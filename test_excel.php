<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
/** Error reporting */
error_reporting(E_ALL);
date_default_timezone_set("Asia/Bangkok");

// echo '555';
//
// $user = "root";
// $pass = "password";
// $db_name = "3bb_standby";
// $hostname = "10.11.11.210";
// $conn=mysqli_connect($hostname,$user,$pass,$db_name);
//
// mysqli_query("SET NAMES UTF8");

// $sql_query=mysqli_query($conn,"SELECT * from escalation_level_one escal_1");
//
// while($result=mysqli_fetch_array($sql_query)) {
// 	echo $result['escalation_name'];
// 	echo "<br>";
// }
//
/** PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'TEST');
//
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'CustomerID')
            ->setCellValue('B1', 'Name')
            ->setCellValue('C1', 'Email')
			->setCellValue('D1', 'CountryCode')
			->setCellValue('E1', 'Budget')
            ->setCellValue('F1', 'Used');
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:B3');
    $objPHPExcel->getActiveSheet()->setCellValue('A2', 'เทส');
    $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue('B2', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('C2', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('D2', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('E2', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('F2', 'เทส');

	// $objPHPExcel->getActiveSheet()->setCellValue('A3', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('B3', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('C3', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('D3', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('E3', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('F3', 'เทส');
	$objPHPExcel->getActiveSheet()->setCellValue('G3', 'เทส2');
	$objPHPExcel->getActiveSheet()->setCellValue('H3', 'เทส3');
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>
